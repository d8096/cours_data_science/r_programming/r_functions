

# TD + cours sur fonctions R

## Cours

Structures de contrôle, boucles, vectorisation (apply), définition de
fonctions

## TD à déposer sur git pour le 13/11/2022

### Données

Données d'anesthésie

### Consignes générales

-   Ajout sur git : Frok, git clone, git push (une fois terminé)
-   Sortie .html du fichier .rmd

### fonctions.R

-   Load packages
-   Créer 2 fonctions propres :
    -   « data_management » :

        -   Apply : formatter les colonnes en nombre / charactère

        -   Remplacer dans la colonne Sexe : homme / femme

        -   → sortie de la fonction : dataframe

    -   « aggrega » :

        -   Calculer pour chaque modalité du Sexe (Homme/Femme) : age
            moyen, poids moyen, moyenne de la pam, durée moyenne de
            l'anesthésie

        -   → Sortie de la fonction : dataframe

### execute.rmd

-   Load data et fonctions.R
-   Appel des fonctions :
    -    « data_management »

    -   « aggrega »

    -   Afficher le dataframe en sortie de la fonction « aggrega »

    -   Tracer un graphique permettant de comparer la moyenne de la pam
        en fonction du sexe
